var fs = require('fs');
var path = require('path');
var config = require('../config/config');

function renderStaticHtmlFile(path) {
    return function(req, res, next) {
        fs.createReadStream(__dirname + '/../../frontend/' + path)
            .pipe(res);
    }
}

module.exports = {
    index          : renderStaticHtmlFile('dist/index.html'),
    swagger        : renderStaticHtmlFile('swagger/index.html')
};
