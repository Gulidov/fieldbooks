'use strict';
const _              = require('lodash'),
      moment         = require('moment'),
      co             = require('co'),
      BaseController = require('./baseController'),
      validator      = require('./../../utils/validator').validator,
      helper         = require('./../../utils/helperService'),
      logger         = require('./../../utils/logger'),
      config         = require('./../../config/config'),
      db             = require('./../../models'),
      bcrypt         = require('bcryptjs'),
      del            = require('del');

let startTransaction = helper.startTransaction;

class UsersController extends BaseController {
    constructor(options) {
        super(options);
        this.auth = [this._validateAuth, this._auth];
        this.getCurrentUser = [this._getCurrentUser];
        this.modelDelete = [this._validateModelDelete.bind(this), UsersController.modelDeleteAcl, this._modelDelete.bind(this)];
        this.test = [this._test];
        this.createUsers = [this._createUsers];
        this.getCurrent = [this._getCurrent];
        this.logout = [this._logout];
        helper.emitter.on('db:loadEnd', ()=> {
            this.options.model = db.models.User;
        });
    }

    // actions

    _createUsers(req, res, next) {
        co(function *() {

           /* var data = {
                username  : req.body.username,
                password  : req.body.password
            };*/

            /*let userExists = yield {
                user: db.models.User.findOne({email: data.username}),
            };

            if (userExists.user) {
                let error = new Error('The user with this username exists');
                error.status = 400;
                throw error;
            }

            let user = yield db.models.User.create(data);
            user = user.toJSON({full: true});
            user.toJSON = undefined;
            let result = {
                data: user
            };

            res.status(201).json(result);*/

            res.status(201).json({});
        }).catch(function(err) {
            next(err);
        });
    }

    _logout(req,res, next) {
        req.session.destroy(function (err) {
            res.status(201).json({});
        });
    }

    _getCurrent(req, res, next) {
        co(function *() {
            if (!req.user) {
                let err = new Error('User not authorized. Please sign in');
                err.status = 401;
                return next(err);
            }

            res.status(201).json(req.user);
        }).catch(function(err) {
            next(err);
        });
    }

    _modelUpdate(req, res, next) {
        co(function *() {
            let data = {
                email     : req.body.email,
                first_name: req.body.first_name,
                last_name : req.body.last_name
            };
            if (req.user.role === 'admin') {
                data.is_blocked = req.body.is_blocked;
                data.discount = req.body.discount;
            }

            let user = yield db.models.User.findOne(req.params.id);
            if (!user) {
                let err = new Error('User does not exist');
                err.status = 404;
                return next(err);
            }
            let userExists = yield {
                user: db.models.User.findOne({email: data.email, id: {not: req.params.id}}),
                host: db.models.Host.findOne({email: data.email})
            };
            if (userExists.user || userExists.host) {
                let error = new Error('The email address you have entered is already registered');
                error.status = 400;
                throw error;
            }

            yield db.models.User.update(user.id, data);
            let filters = helper.getFilters(req, db.models.User);
            let resultUser = yield db.models.User.findOne(user.id).populateSome(filters.populate);
            let result = {
                data: resultUser
            };
            res.json(result);
        }).catch(function(err) {
            next(err);
        });
    }
}

module.exports = {
    instance: new UsersController(),
    class   : UsersController
};