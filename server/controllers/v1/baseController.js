'use strict';
var _                   = require('lodash'),
    moment              = require('moment'),
    co                  = require('co'),
    modelsSaveService   = require('./../../utils/modelsSaveService'),
    modelsUpdateService = require('./../../utils/modelsUpdateService'),
    modelsDeleteService = require('./../../utils/modelsDeleteService'),
    modelsGetService    = require('./../../utils/modelsGetService').getModel,
    validator           = require('./../../utils/validator').validator,
    logger              = require('./../../utils/logger'),
    config              = require('./../../config/config'),
    db                  = require('./../../models');

class BaseController {
    constructor(options) {
        this.options = options || {};
        this.modelGet = [this._validateModelGet.bind(this), this._modelGet.bind(this)];
        this.modelCreate = [this._validateModelCreate, this._modelCreate];
        this.modelUpdate = [this._checkPermissions, this._validateModelUpdate, this._modelUpdate];
        this.modelPatch = [this._checkPermissions, this._validateModelPatch, this._modelPatch];
        this.modelDelete = [this._checkPermissions, this._validateModelDelete.bind(this), this._modelDelete.bind(this)];
    }

    // validation
    _validateModelGet(req, res, next) {
        req.checkParams('id', 'only positive int valid').optional().isPositiveInt();
        validator(req, res, next);
    }

    _validateModelDelete(req, res, next) {
        req.checkParams('id', 'only positive int valid').isPositiveInt();
        validator(req, res, next);
    }

    _validateShow(req, res, next) {
        req.checkParams('id', 'only positive int valid').isPositiveInt();
        validator(req, res, next);
    }

    // actions
    _modelGet(req, res, next) {
        modelsGetService(req, res, next, this.options);
    }

    _modelDelete(req, res, next) {
        modelsDeleteService(req, res, next, this.options);
    }
}

module.exports = BaseController;
