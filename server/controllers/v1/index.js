'use strict';

module.exports = {
    users           : require('./usersController').instance,
    goalscorers     : require('./goalscorersController').instance,
    teams           : require('./teamsController').instance,
    webhooks           : require('./webhooksController').instance
};
