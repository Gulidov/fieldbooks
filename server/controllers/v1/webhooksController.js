'use strict';
const _              = require('lodash'),
    moment         = require('moment'),
    co             = require('co'),
    BaseController = require('./baseController'),
    validator      = require('./../../utils/validator').validator,
    helper         = require('./../../utils/helperService'),
    logger         = require('./../../utils/logger'),
    db             = require('./../../models'),
    config         = require('./../../config/config'),
    Fieldbook      = require('node-fieldbook');

let book = new Fieldbook({
    username: 'key-1',
    password: 'ScOpBg-5s7q3P56UBR9W',
    book: '570f8b5921f9ec0300265b7b'
});

let spreadsheetModels = {};

class WebhooksController extends BaseController {
    constructor(options) {
        super(options);
        this.getWebhooks = [this._getWebhooks];
        this.addWebhook = [this._addWebhook];
        this.syncWebhook = [this._syncWebhook];
        this.removeWebhook = [this._removeWebhook];
        helper.emitter.on('db:loadEnd', ()=> {
            spreadsheetModels.teams = db.models.Team;
            spreadsheetModels.users = db.models.User;
            spreadsheetModels.goalscorers = db.models.Goalscorer;
        });
    }

    _addWebhook(req, res, next) {
        co(function *() {

            let webhook = yield book.addWebhook({
                url: config.server.baseURL + '/api/v1/webhooks/sync', //Basic Auth: https://user:pass@example.com/callback
                actions: ['create', 'update', 'destroy']
            });

            console.log(webhook);

            if (webhook) {
                return res.json(webhook);
            }

            res.json({});
        }).catch(err => next(err));
    }

    _getWebhooks(req, res, next) {
        co(function *() {

            let webhooks = yield book.getWebhooks({});

            if (webhooks) {
                return res.json(webhooks);
            }

            res.json({});
        }).catch(err => next(err));
    }

    _removeWebhook(req, res, next) {
        co(function *() {

            let webhookId = req.query.webhookId;

            let webhookRes = yield book.deleteWebhook(webhookId);
            console.log(webhookRes);
            if (webhookRes) {
                return res.json(webhookRes);
            }

            res.json({});
        }).catch(err => next(err));
    }

    _syncWebhook(req, res, next) {
        co(function *() {

            let objName = _.keys(req.body.changes)[0];
            let destroyObjects = [];
            let updateObjects = [];
            let createObjects = [];
            let promises = [];
            let resultCreating = false;

            if (typeof req.body.changes[objName].destroy != 'undefined') {
                _.forEach(req.body.changes[objName].destroy, function(object, i) {
                    let id = object.id.split(" ");
                    id = id[1];
                    destroyObjects.push(id);
                });

                yield spreadsheetModels[objName].remove({spreadsheet_id: destroyObjects});
            }

            if (typeof req.body.changes[objName].update != 'undefined') {
                _.forEach(req.body.changes[objName].update, function(object, i) {
                    let id = object.id.split(" ");
                    id = id[1];

                    let updatingObj = object;
                    delete updatingObj.id;
                    delete updatingObj.record_url;

                    let promise = spreadsheetModels[objName].update({spreadsheet_id: id}, updatingObj);
                    promises.push(promise);
                });

                yield promises;
            }

            if (typeof req.body.changes[objName].create != 'undefined') {
                _.forEach(req.body.changes[objName].create, function(object, i) {
                    let id = object.id.split(" ");
                    id = id[1];

                    let creatingObj = object;
                    delete creatingObj.id;
                    delete creatingObj.record_url;
                    creatingObj.spreadsheet_id = id;
                    createObjects.push(creatingObj);
                });

                resultCreating = yield spreadsheetModels[objName].create(createObjects);
            }

            res.json({});
        }).catch(err => next(err));
    }
}

module.exports = {
    instance: new WebhooksController(),
    class   : WebhooksController
};
