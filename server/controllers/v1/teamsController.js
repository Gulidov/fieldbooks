/**
 * Created by sergey on 4/14/16.
 */
'use strict';
var _              = require('lodash'),
    moment         = require('moment'),
    co             = require('co'),
    BaseController = require('./baseController'),
    validator      = require('./../../utils/validator').validator,
    helper         = require('./../../utils/helperService'),
    logger         = require('./../../utils/logger'),
    db             = require('./../../models');

class TeamsController extends BaseController {
    constructor(options) {
        super(options);
        this.changeData = [this._changeData];
        this.getTeams = [this._getTeams];
        helper.emitter.on('db:loadEnd', ()=> {
            this.options.model = db.models.Team;
        });
    }

    // actions
    /*    _modelCreate(req, res, next) {
     co(function *() {
     let listing = yield db.models.Listing.findOne(req.body.listing);
     if (!listing) {
     let err = new Error('Entity listing not found');
     err.status = 404;
     return next(err);
     }

     var data = {
     feedback: !_.isUndefined(req.body.feedback) ? req.body.feedback : null,
     // round down value, ex.: 1.7 - 1.5
     rate    : Math.floor(Number(req.body.rate.toFixed(2)) * 2) / 2,
     user    : req.user.id,
     listing : listing.id,
     host    : listing.host
     };
     data = _.omit(data, _.isNull);
     let feedback = yield db.models.Feedback.create(data);
     let result = {
     data: feedback
     };
     res.status(201).json(result);
     }).catch(err => next(err));
     }*/

    _changeData(req, res, next) {
        co(function *() {

            let result = {
                data: {}
            };
            res.json(result);
        }).catch(err => next(err));
    }

    _getTeams(req, res, next) {
        co(function *() {

            let teams = yield db.models.Team.find();

            res.json(teams);
        }).catch(err => next(err));
    }

    
    /*    _modelUpdate(req, res, next) {
     co(function *() {
     var data = {
     feedback: !_.isUndefined(req.body.feedback) ? req.body.feedback : null,
     // round down value, ex.: 1.7 - 1.5
     rate    : !_.isUndefined(req.body.rate) ? Math.floor(Number(req.body.rate.toFixed(2)) * 2) / 2 : null,
     user    : req.user.id
     };
     data = _.omit(data, _.isNull);

     let listing = yield db.models.Listing.findOne(req.body.listing);
     if (!listing) {
     let err = new Error('Entity listing not found');
     err.status = 404;
     return next(err);
     }
     data.listing = listing.id;
     data.host = listing.host;

     let feedbacks = yield db.models.Feedback.update(req.params.id, data);
     let feedback = feedbacks[0];
     if (!feedback) {
     let err = new Error('Entity not found');
     err.status = 404;
     return next(err);
     }
     let result = {
     data: feedback
     };
     res.json(result);
     }).catch(err => next(err));
     }*/
}

module.exports = {
    instance: new TeamsController(),
    class   : TeamsController
};
