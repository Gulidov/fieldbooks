'use strict';

var controllers        = require('./../controllers'),
    passport           = require('passport'),
    auth               = require('./../utils/auth').auth,
    checkAuth          = require('./../utils/auth').checkAuth,
    initAuth           = require('./../utils/auth').init,
    updateOnlineStatus = require('./../utils/userHelpers').updateOnlineStatus,
    config             = require('./../config/config'),
    multiparty         = require('connect-multiparty'),
    db                 = require('./../models'),
    co                 = require('co'),
    redis              = require('redis').createClient(),
    session            = require('express-session'),
    redisStore         = require('connect-redis')(session),
    multi              = multiparty({maxFilesSize: config.multiparty.fileSize});

module.exports = function(app) {
    app.use(session({
        store: new redisStore({
            host: 'localhost',
            port: 6397,
            client: redis
        }),
        secret: 'secretysecret'
    }));
    app.use(passport.initialize());
    initAuth();

    // static links
    app.get('/', controllers.static.index);

    /**
     * Login
     */
    app.post('/api/v1/auth', auth, function (req, res, next) {
        res.send({});
    });

    /**
     * Logout
     */
    app.post('/api/v1/logout', controllers.v1.users.logout);

    // static links
    app.get('/', controllers.static.index);
    //app.get('/index', controllers.static.index);
    //app.get('/api/doc', controllers.static.swagger);

    /**
     * Users
     */
    app.route('/api/v1/users')
        .get(checkAuth, controllers.v1.users.modelGet)
        .put(controllers.v1.users.createUsers)
        .post(checkAuth, function (req, res) {
            res.send({});
        });

    app.route('/api/v1/users/current')
        .get(checkAuth, controllers.v1.users.getCurrent);


    /**
     * Goalscorers
     */
    app.route('/api/v1/goalscorers')
        .get(checkAuth, controllers.v1.goalscorers.getGoalscorers);

    /**
     * Teams
     */
    app.route('/api/v1/teams')
        .get(checkAuth, controllers.v1.teams.getTeams);

    /**
     * Webhooks
     */
    app.route('/api/v1/webhooks')
        .get(controllers.v1.webhooks.getWebhooks)
        .post(controllers.v1.webhooks.addWebhook)
        .delete(controllers.v1.webhooks.removeWebhook);

    app.route('/api/v1/webhooks/sync')
        .post(controllers.v1.webhooks.syncWebhook);

    //app.route('/api/v1/users')
    //    .get(checkAuth, controllers.v1.users.modelGet)
    //    .post(controllers.v1.users.modelCreate);
    //
    //app.route('/api/v1/me')
    //    .get(auth, controllers.v1.users.getCurrentUser);
    //
    //app.route('/api/v1/users/:id')
    //    .get(auth, controllers.v1.users.modelGet)
    //    .put(auth, controllers.v1.users.modelUpdate)
    //    .patch(auth, controllers.v1.users.modelPatch)
    //    .delete(auth, controllers.v1.users.modelDelete);

    /**
     * Feedbacks
     */
    //app.route('/api/v1/feedbacks')
    //    .get(controllers.v1.feedbacks.modelGet)
    //    .post(auth, controllers.v1.feedbacks.modelCreate);
    //
    //app.route('/api/v1/feedbacks/check_is_create_allowed')
    //    .post(auth, controllers.v1.feedbacks.checkIsCreateAllowed);
    //
    //app.route('/api/v1/feedbacks/:id')
    //    .get(controllers.v1.feedbacks.modelGet)
    //    .put(auth, controllers.v1.feedbacks.modelUpdate)
    //    .patch(auth, controllers.v1.feedbacks.modelPatch)
    //    .delete(auth, controllers.v1.feedbacks.modelDelete);

    app.get('*', controllers.static.index);
};
