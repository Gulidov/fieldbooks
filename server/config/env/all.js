'use strict';
var protocol = 'http',
    host     = process.env.HOST || 'localhost',
    port     = process.env.PORT || '3000',
    baseUrl  = `${protocol}://${host}:${port}`,
    rootPath = `${__dirname}/../../..`;

process.env.TMPDIR = `${rootPath}/.tmp`;

module.exports = {
    server    : {
        protocol: protocol,
        host    : host,
        port    : port,
        baseURL : baseUrl,
        root    : rootPath,
        temp    : `${rootPath}/.tmp`
    },
    app       : {},
    multiparty: {
        fileSize: 10 * 1024 * 1024 // 10 mb
    }
};