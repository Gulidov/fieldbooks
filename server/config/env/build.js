'use strict';
var protocol = 'http',
    host     = process.env.HOST || '0.0.0.0',
    port     = process.env.PORT || '80',
    config   = require('./../../../config.json'),
    rootPath = `${__dirname}/../../..`;

process.env.TMPDIR = `${rootPath}/.tmp`;

module.exports = {
    server  : {
        protocol: protocol,
        host    : host,
        port    : port,
        baseURL : 'http://52.88.26.141',
        root    : rootPath,
        temp    : `${rootPath}/.tmp`
    },
    db      : config.build.db,
    app     : {}
};
