'use strict';
var protocol = 'http',
    host     = process.env.HOST || '148.251.187.5',
    port     = process.env.PORT || '3007',
    baseUrl  = `${protocol}://${host}:${port}`,
    config   = require('./../../../config.json'),
    rootPath = `${__dirname}/../../..`;

process.env.TMPDIR = `${rootPath}/.tmp`;

module.exports = {
    server  : {
        protocol: protocol,
        host    : host,
        port    : port,
        baseURL : 'http://redmine.cleveroad.com:3007',
        root    : rootPath,
        temp    : `${rootPath}/.tmp`
    },
    db      : config.stage.db,
    app     : {}
};
