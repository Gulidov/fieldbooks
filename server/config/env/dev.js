'use strict';
var protocol = 'http',
    host     = process.env.HOST || 'localhost',
    port     = process.env.PORT || '3000',
    baseUrl  = `${protocol}://${host}:${port}`,
    config   = require('./../../../config.json'),
    rootPath = `${__dirname}/../../..`;

process.env.TMPDIR = `${rootPath}/.tmp`;

module.exports = {
    server  : {
        protocol: protocol,
        host    : host,
        port    : port,
        baseURL : 'http://localhost:3000',
        root    : rootPath,
        temp    : `${rootPath}/.tmp`
    },
    db      : config.dev.db,
    app     : {}
};