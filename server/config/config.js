'use strict';
var _ = require('lodash');

var env = process.env.NODE_ENV || 'dev';
// Load server configuration
var config = _.extend(
    require(`${__dirname}/env/all`),
    require(`${__dirname}/env/${env}`) || {});

if (config.db.logQueries) {
    process.env.LOG_QUERIES = true;
}

module.exports = config;
