var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var expressValidator = require('express-validator');
var errorHandler = require('./utils/errorHandler');
var requestHandler = require('./utils/requestHandler');
var disallowMethods = require('./utils/methodsHandler');
var validator = require('./utils/validator');
var urlManager = require('./routes');
var config = require('./config/config');
var favicon = require('serve-favicon');

//initialize the app
var app = module.exports = express();
var env = process.env.NODE_ENV;
app.set('env', env);

//app.use(cors());

//set up static files directory
app.use(express.static(__dirname + '/../frontend'));
//app.use(favicon(__dirname + '/../frontend/favicon.ico'));
app.use(requestHandler);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(expressValidator(validator.options));

urlManager(app);

disallowMethods(app);

//set up http error handler
app.use(errorHandler(app));

process.on('uncaughtException', function(err, req, res) {
    console.log(err.stack);
});

process.on('SIGINT', function() {
    // calling shutdown allows your process to exit normally
    process.exit();
});