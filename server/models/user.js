'use strict';

const Waterline        = require('./../../node_modules/sails-mysql-transactions/waterline'),
      Base             = require('./base'),
      logger           = require('./../utils/logger'),
      config           = require('./../config/config'),
      helper           = require('./../utils/helperService'),
      startTransaction = require('./../utils/helperService').startTransaction,
      moment           = require('moment'),
      bcrypt           = require('bcryptjs'),
      _                = require('lodash'),
      co               = require('co');

let User = {
    identity : 'user',
    tableName: 'users',

    attributes     : {
        created_at        : {
            type      : 'datetime',
            defaultsTo: function() {
                return moment.utc()._d;
            }
        },
        updated_at        : {
            type      : 'datetime',
            defaultsTo: function() {
                return moment.utc()._d;
            }
        },
        password          : {
            type     : 'string',
            notNull  : true,
            minLength: 6,
            maxLength: 255,
            required : true
        },
        username        : {
            type     : 'string',
            minLength: 3,
            maxLength: 255
        },
        spreadsheet_id: {
            type: 'integer',
            notNull: true,
            unique: true
        },
        // instance methods
        validPassword     : function(password) {
            return bcrypt.compareSync(password, this.password);
        },
        isCurrUserRequest : function(req) {
            return Number(req.params.id) === req.user.id;
        },
        toJSON            : function(options) {
            options = options || {};
            options.model = models.User;
            return Base.attributes.toJSON.call(this, options);
        }
    },
    allowPopulate  : [],
    publicProps    : ['id', 'created_at', 'updated_at', 'username'],
    strIsHash      : function(str) {
        return str.startsWith('$2a$10$') && str.length === 60;
    },
    beforeCreate   : function(attrs, next) {
        attrs = this.trim(attrs);
        let salt = bcrypt.genSaltSync();
        attrs.password = bcrypt.hashSync(attrs.password, salt);
        next();
    },
    beforeUpdate   : function(attrs, next) {
        attrs = this.trim(attrs);
        let salt = bcrypt.genSaltSync();
        attrs.password = bcrypt.hashSync(attrs.password, salt);
        attrs.updated_at = moment.utc()._d;
        next();
    },
    allowQuery     : {}
};

let instance = _.defaultsDeep(User, Base);
module.exports = Waterline.Collection.extend(instance);
