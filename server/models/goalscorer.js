'use strict';

var Waterline = require('./../../node_modules/sails-mysql-transactions/waterline'),
    Base      = require('./base'),
    co        = require('co'),
    logger    = require('./../utils/logger'),
    config    = require('./../config/config'),
    moment    = require('moment'),
    _         = require('lodash');

let Goalscorer = {
    identity : 'goalscorer',
    tableName: 'goalscorers',

    attributes: {
        created_at: {
            type      : 'datetime',
            defaultsTo: function() {
                return moment.utc()._d;
            }
        },
        updated_at: {
            type      : 'datetime',
            defaultsTo: function() {
                return moment.utc()._d;
            }
        },
        name  : {
            type    : 'text'
        },
        scores      : {
            type: 'integer'
        },
        spreadsheet_id: {
            type: 'integer',
            notNull: true,
            unique: true
        }
    },
    allowPopulate: []
};

let instance = _.defaultsDeep(Goalscorer, Base);
module.exports = Waterline.Collection.extend(instance);
