'use strict';

//var Waterline     = require('./../../node_modules/sails-mysql-transactions/waterline'),
var Waterline     = require('waterline'),
    diskAdapter   = require('sails-disk'),
    //mysqlTAdapter = require('sails-mysql'),
    mysqlTAdapter = require('sails-mysql-transactions'),
    config        = require(__dirname + '/../config/config'),
    helper        = require('./../utils/helperService'),
    _             = require('lodash');

var dbConfig = {
    // Setup Adapters
    // Creates named adapters that have been required
    adapters: {
        'default': diskAdapter,
        disk     : diskAdapter,
        mysql    : mysqlTAdapter
    },

    // Build Connections Config
    // Setup connections using the named adapter configs
    connections: {
        disk      : {
            adapter: 'disk'
        },
        mysql     : {
            adapter : 'mysql',
            host    : config.db.host,
            user    : config.db.user,
            password: config.db.password,
            database: config.db.dbName,
            charset : config.db.charset
        }
    },
    defaults   : {
        //migrate                   : 'safe',
        //transactionConnectionLimit: 10,
        //rollbackTransactionOnError: true,
        //queryCaseSensitive        : false
    }
};

var orm = new Waterline();

// Load the Models into the ORM
orm.loadCollection(require('./user'));
orm.loadCollection(require('./team'));
orm.loadCollection(require('./goalscorer'));

function initDb() {
    return new Promise((resolve, reject) => {
        orm.initialize(dbConfig, function(err, models) {
            if (err) {
                return reject(err);
            }

            global.models = module.exports.models = {
                Team              : models.collections.team,
                Goalscorer              : models.collections.goalscorer,
                User                  : models.collections.user
            };
            module.exports.connections = models.connections;
            helper.emitter.emit('db:loadEnd');
            resolve();
        });
    });
}

module.exports = {
    initDb: initDb
};
