'use strict';

var Waterline = require('waterline'),
    logger    = require('./../utils/logger'),
    config    = require('./../config/config'),
    _         = require('lodash'),
    moment    = require('moment'),
    co        = require('co');

let Base = {
    connection   : 'mysql',
    autoPK       : true,
    autoCreatedAt: false,
    autoUpdatedAt: false,
    migrate      : config.db.migrate,

    attributes      : {
        toJSON: function(options) {
            let filtered = this.toObject(),
                isFullModel = options && options.model && options.full,
                publicProps = options && options.model ? options.model.publicProps : null;
            delete filtered.password;
            delete filtered.inspect;
            if (!isFullModel && publicProps) {
                filtered = _.pick(filtered, publicProps);
            }
            if (options && options.model) {
                filtered = options.model.populateToObject(filtered);
            }
            return filtered;
        }
    },
    allowPopulate   : [],
    populateToObject: function(model) {
        _.forEach(this.allowPopulate, function(prop) {
            if (_.isNumber(model[prop])) {
                model[prop] = {id: model[prop]};
            }
        });
        return model;
    },
    truncate        : function() {
        return new Promise((resolve, reject) => {
            let query = `TRUNCATE TABLE ${this.tableName}`;
            this.query(query, function(err, results) {
                if (err) {
                    return reject(err);
                }
                resolve();
            });
        })
    },
    build           : function(attrs) {
        return new this._model(attrs);
    },
    buildEach       : function(data) {
        return _.map(data, function(attrs) {
            return new this._model(attrs);
        }, this);
    },
    trim            : function(model) {
        _.forOwn(model, function(value, key) {
            model[key] = _.isString(model[key]) ? model[key].trim() : model[key];
        });
        return model;
    },
    findWithFilters: function(options) {
        return co(function *() {
            //let {query, filters} = options;

            let query = options.query,
                filters = options.filters;
            return yield {
                data: this.find(query)
                    .populateSome(filters.populate)
                    .paginate({page: filters.paginate.page, limit: filters.paginate.limit})
            };
        }.bind(this));
    },
    findWithFiltersAndCount: function(options) {
        return co(function *() {
            //let {query, filters} = options;

            let query = options.query,
                filters = options.filters;
            return yield {
                data: this.find(query)
                    .populateSome(filters.populate)
                    .paginate({page: filters.paginate.page, limit: filters.paginate.limit}),
                count: this.count(query)
            };
        }.bind(this));
    },
    beforeCreate    : function(attrs, next) {
        attrs = this.trim(attrs);
        attrs.updated_at = moment.utc()._d;
        next();
    },
    beforeUpdate    : function(attrs, next) {
        attrs = this.trim(attrs);
        attrs.updated_at = moment.utc()._d;
        next();
    }
};

module.exports = Base;
