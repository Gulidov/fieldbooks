'use strict';
var _ = require('lodash');
var expressValidator = require('express-validator').validator;

function validator(req, res, next) {
    var errors = req.validationErrors();
    if (errors) {
        let error = errors[0];
        let message = `param: ${error.param} - ${error.msg}`;
        return res.status(400).json({
            error  : 400,
            details: message
        })
    }
    next();
}

var options = {
    customValidators: {
        isIntArr: function(values) {
            if (!values || values && !values.length) {
                return false;
            }
            return _.every(values, function(value) {
                return expressValidator.isInt(value) && value > 0;
            });
        },
        isArr: function(values) {

            if(!values)
                return false;

            if(values.length === 0)
                return _.isArray(values);

            return _.every(values, function(value) {
                return expressValidator.isInt(value) && value > 0;
            });
        },
        isIntOrNull: function(value) {
            return expressValidator.isInt(value) || expressValidator.isNull(value);
        },
        isPositiveInt: function(value) {
            return expressValidator.isInt(value) && value > 0;
        },
        eachLen: function(values, prop, param) {
            if (!_.isArray(values)) {
                return false;
            }
            return  _.every(values, function(val) {
                return expressValidator.isLength(val[prop], param);
            });
        },
        eachIsEmail: function(values, prop) {
            if (!_.isArray(values)) {
                return false;
            }
            return _.every(values, function(val) {
                return expressValidator.isEmail(val[prop]);
            });
        },
        eachIsPositiveIntOptional: function(values, prop) {
            if (!_.isArray(values)) {
                return false;
            }
            return _.every(values, function(val) {
                if (val[prop]) {
                    return expressValidator.isInt(val[prop]) && val[prop] > 0;
                }
                return true;
            });
        },
        isArrayLen: function(values, param) {
            return _.isArray(values) && values.length === param;
        }
    }
};

module.exports = {
    validator: validator,
    options  : options
};