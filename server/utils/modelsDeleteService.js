'use strict';
var config = require('./../config/config'),
    _      = require('lodash'),
    co     = require('co');

function deleteModel(req, res, next, options) {
    co(function *() {
        var Model = options.model;
        let removed = yield Model.destroy(req.params.id);
        if (!removed.length) {
            let err = new Error('Entity not found');
            err.status = 404;
            return next(err);
        }
        res.status(204).json();
    }).catch(function(err) {
        next(err);
    });
}

module.exports = deleteModel;