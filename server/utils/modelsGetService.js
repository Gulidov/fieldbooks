'use strict';
let config     = require('./../config/config'),
    db         = require('./../models'),
    helper     = require('./helperService'),
    _          = require('lodash'),
    co         = require('co');

function getModels(options) {
    return co(function *() {
        //let {req, Model, filters, populate=[]} = options;
        let req = options.req,
            Model = options.Model,
            filters = options.filters,
            populate = options.populate || [];

        let models,
            result = {},
            full   = req.fullModel || null,
            query  = helper.populateQuery(req.query, Model);
        filters = filters || helper.getFilters(req, Model);

        let queryCondition = req.queryCondition || {};
        query = _.defaultsDeep(queryCondition, query);
        filters.populate = populate.length ? _.uniq(filters.populate.concat(populate)) : filters.populate;

        if (req.query.total_count === 'true') {
            result.meta = {};
            models = yield Model.findWithFiltersAndCount({query: query, filters: filters});
            result.meta.total_count = models.count;
        } else {
            models = yield Model.findWithFilters({query: query, filters: filters});
        }

        result.data = helper.toJSON({models: models.data, dbModel: Model, full: full});
        return result;
    });
}

function getModel(req, res, next, options) {
    co(function *() {
        let Model   = options.model,
            result  = {},
            full    = req.fullModel || null,
            filters = helper.getFilters(req, Model);

        if (req.params.id) {
            let model = yield Model.findOne(req.params.id).populateSome(filters.populate);
            if (!model) {
                let err = new Error('Entity not found');
                err.status = 404;
                return next(err);
            }

            result.data = helper.toJSON({model: model, dbModel: Model, full: full});
            res.json(result);
        } else {
            result = yield getModels({req: req, Model: Model, filters: filters});
            res.json(result);
        }
    }).catch(function(err) {
        next(err);
    });
}

module.exports = {getModel, getModels};