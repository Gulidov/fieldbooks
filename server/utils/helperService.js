'use strict';
let _             = require('lodash'),
    config        = require('./../config/config'),
    logger        = require('./logger'),
    knox          = require('knox'),
    gm            = require('gm'),
    co            = require('co'),
    events        = require('events'),
    emitter       = new events.EventEmitter(),
    Transaction   = require('sails-mysql-transactions').Transaction;

emitter.setMaxListeners(Infinity);

function getFilters(req, model) {
    let allowPopulate = model.allowPopulate;
    let populateFields = req.query.populate ? req.query.populate.split(',') : [];
    let populate = [];
    if (populateFields[0] === 'all') {
        populate = allowPopulate;
    } else {
        _.forEach(populateFields, function(fieldName) {
            if (allowPopulate.indexOf(fieldName) !== -1) {
                populate.push(fieldName);
            }
        })
    }
    let page  = Number(req.query.page) || 1,
        limit = Number(req.query.limit) || 20;

    return {
        populate: populate,
        paginate: {
            page : page,
            limit: limit
        }
    };
}

function toJSON(options) {
    //let {models, model, dbModel, full} = options;
    let models = options.models,
        model = options.model,
        dbModel = options.dbModel,
        full = options.full;
    let opts = {
        model: dbModel,
        full : full
    };
    if (model) {
        model = model.toJSON(opts);
        model.toJSON = undefined;
        return model;
    }
    return _.map(models, function(model) {
        model = model.toJSON(opts);
        model.toJSON = undefined;
        return model;
    });
}

function startTransaction() {
    return new Promise((resolve, reject) => {
        Transaction.start(function(err, transaction) {
            if (err) {
                return reject({error: err, transaction: transaction});
            }
            resolve(transaction);
        });
    });
}

function populateQuery(query, model) {
    var result = {};
    _.map(query, function(value, key) {
        if (model.allowQuery && model.allowQuery[key]) {
            switch (model.allowQuery[key]) {
                case 'boolean':
                    result[key] = value === 'true' ? true : false;
                    break;
                case 'string':
                    result[key] = value.toString();
                    break;
                case 'number':
                    result[key] = parseFloat(value);
                    break;
            }
        }
    });
    return result;
}

module.exports = {
    getFilters      : getFilters,
    emitter         : emitter,
    startTransaction: startTransaction,
    toJSON          : toJSON,
    populateQuery   : populateQuery
};
