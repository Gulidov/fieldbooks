'use strict';
var LocalStrategy          = require('passport-local').Strategy,
    co                     = require('co'),
    logger                 = require('./../utils/logger'),
    config                 = require('./../config/config'),
    db                     = require('./../models'),
    moment                 = require('moment'),
    _                      = require('lodash'),
    passport               = require('passport'),
    bcrypt                 = require('bcryptjs');


function init() {
    passport.serializeUser(function(user, done) {
        logger.log('info', 'passport.serializeUser');
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        logger.log('info', 'passport.deserializeUser');
        done(null, user);
    });

    passport.use(new LocalStrategy(/*{
            passReqToCallback: true
        },*/
        function(/*req, */username, password, done) {
            // console.log('HUYOOVOOOOOOOOOOOOOOOOOOOO');
            // return;

            // let salt = bcrypt.genSaltSync(10);
            // let hash = bcrypt.hashSync('1111', salt);
            // console.log(hash);
            // return;
            logger.log('info', 'passport.LocalStrategy');
            co(function *() {
                let users = yield {
                    user: db.models.User.findOne({username: username}),
                };
                let user = users.user;

                if (!user) {
                    let error = new Error();
                    error.message = 'Invalid username';
                    error.status = 401;
                    return done(error);
                }

                if (!bcrypt.compareSync(password, user.password)) {
                    var error = new Error();
                    error.status = 401;
                    error.message = 'password incorrect!';
                    return done(error);
                }

                let userObj = {
                    id: user.id,
                    username: user.username
                };

                done(null, userObj);
            }).catch(function(err) {
                done(err);
            });
        })
    );
}

// authentication middleware
function checkAuth(req, res, next) {
    var staticFiles = req.path.search(/.+.(jpg|jpeg|gif|png|css|js|ico|xml|rss|txt|woff)$/) !== -1;

    if (staticFiles) {
        return next();
    }

    //req.session.passport.user
    if (!req.session) {
        let error = new Error();
        error.message = 'User not authorized';
        error.status = 401;
        return next(error);
    }


    req.user = req.session.passport.user;
    next();

//check session and return error 401
    //transfer passport.authenticate to another function (post)
}

function auth(req, res, next) {
    passport.authenticate('local', {session: true})(req, res, next);
}

module.exports = {
    init            : init,
    checkAuth       : checkAuth,
    auth            : auth
};
