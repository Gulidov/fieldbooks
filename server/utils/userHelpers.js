'use strict';
var co     = require('co'),
    logger = require('./../utils/logger'),
    moment = require('moment'),
    _      = require('lodash');

function updateOnlineStatus(req, res, next) {
    co(function *() {
        if (req.user) {
            let user = req.user;
            user.online_at = moment.utc()._d;
            yield user.save();
        }
        next();
    }).catch(function(err) {
        next(err);
    });
}

module.exports = {
    updateOnlineStatus: updateOnlineStatus
};
