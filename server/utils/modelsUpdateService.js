'use strict';
var config = require('./../config/config');

function updateModel(req, res, next, options) {
    var Model = options.model;

    var populateFields = req.query.populate ? req.query.populate.split(',') : [];
    var includes = [];
    if (populateFields[0] === 'all') {
        includes = options.populate;
    } else {
        _.forEach(populateFields, function(fieldName) {
            if (options.populate.indexOf(fieldName) !== -1) {
                includes.push(fieldName);
            }
        })
    }

    Model.update({id: req.params.id}, options.data)
        .populateSome(includes)
        .then(function (result) {
            if (next) {
                req.instance = result;
                return next();
            }
            return res.json(result);
        })
        .catch(function (err) {
            err.status = 400;
            next(err);
        });
}

module.exports = updateModel;