var errorHandler = require('express-error-handler');
var logger = require('./logger');

module.exports = function(app) {
    return [
        function(err, req, res, next) {
            if (err) {
                err.status = err.status || 500;
                logger.log('error', err.status, {
                    message: err.message,
                    method : req.method,
                    url    : req.url,
                    body   : req.body,
                    ip     : req.ip
                });
                logger.log('error', err.stack);
                next(err);
            } else {
                next();
            }
        },
        errorHandler({
            server  : app,
            handlers: {
                '500': function err500(err, req, res, next) {
                    res.status(500);
                    if (err.message) {
                        res.send({
                            error  : 500,
                            details: err.message
                        });
                    } else {
                        res.send({
                            error  : 500,
                            details: 'Something unexpected happened. The problem has been logged and we\'ll look into it!'
                        });
                    }
                },
                '501': function err501(err, req, res, next) {
                    res.status(501);
                    if (err.message) {
                        res.send({
                            error  : 501,
                            details: err.message
                        });
                    } else {
                        res.send({
                            error  : 501,
                            details: 'Not Implemented'
                        });
                    }
                },
                '503': function err503(err, req, res, next) {
                    res.status(503);
                    res.send({
                        error  : 503,
                        details: 'We\'re experiencing heavy load, please try again later'
                    });
                },
                '409': function err409(err, req, res, next) {
                    res.status(409);
                    if (err.message) {
                        res.send({
                            error  : 409,
                            details: err.message
                        });
                    } else {
                        res.send({
                            error  : 409,
                            details: "The specified resource already exists"
                        });
                    }
                },
                '405': function err405(err, req, res, next) {
                    res.status(405).send({
                        error  : 405,
                        details: "Method not allowed"
                    });
                },
                '404': function err404(err, req, res, next) {
                    res.status(404);
                    if (err.message) {
                        res.send({
                            error  : 404,
                            details: err.message
                        });
                    } else {
                        res.send({
                            error  : 404,
                            details: "Not Found"
                        });
                    }
                },
                '403': function err403(err, req, res, next) {
                    res.status(403);
                    if (err.message && err.code) {
                        return res.json({
                            error  : 403,
                            code   : err.code,
                            details: err.message
                        });
                    }
                    if (err.message) {
                        return res.json({
                            error  : 403,
                            details: err.message
                        });
                    }
                    return res.json({
                        error  : 403,
                        details: 'Forbidden'
                    });
                },
                '401': function err401(err, req, res, next) {
                    res.status(401);
                    if (err.message) {
                        res.send({
                            error  : 401,
                            details: err.message
                        });
                    } else {
                        res.send({
                            error  : 401,
                            details: "Unauthorized"
                        });
                    }
                },
                '400': function err400(err, req, res, next) {
                    res.status(400);
                    if (err.message) {
                        res.send({
                            error  : 400,
                            details: err.message
                        });
                    } else {
                        res.send({
                            error  : 400,
                            details: "Invalid request"
                        });
                    }
                }
            }
        })]
};
