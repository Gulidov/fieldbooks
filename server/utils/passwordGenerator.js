'use strict';

module.exports = {
    generatePassword: function() {
        var symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let text = "";
        for (let i = 0; i < 7; i++)
            text += symbols.charAt(Math.floor(Math.random() * symbols.length));
        return text;
    }
};