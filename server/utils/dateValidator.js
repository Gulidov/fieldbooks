'use strict';

module.exports = function(date) {

    let newDate = date.split("-");
    return !!(newDate[0].length == 4 && newDate[1].length == 2 && newDate[2].length == 2
    && Number.parseInt(newDate[1]) >= 1 && Number.parseInt(newDate[1]) <= 12
    && Number.parseInt(newDate[1]) >= 1 && Number.parseInt(newDate[1]) <= 12);

};