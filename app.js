"use strict";
require('babel-register');
require('babel-polyfill');
//require('longjohn');

var logger    = require('./server/utils/logger'),
    app       = require('./server/api'),
    config    = require('./server/config/config'),
    https     = require('https'),
    http      = require('http');

var models = require('./server/models');

var httpServer = http.createServer(app);
models.initDb()
    .then(function() {
        httpServer.listen(config.server.port, function() {
            logger.log('info', `web server is available at http://${config.server.host}:${config.server.port}`);
        });
    })
    .catch(function(err) {
        logger.log('error', err.stack);
    });
