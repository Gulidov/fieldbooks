'use strict';
require('babel-register');
require('babel-polyfill');

/**
 * Frontend
 */

const gulp = require('gulp'),
clean      = require('gulp-clean'),
concat     = require('gulp-concat'),
concatCss  = require('gulp-concat-css'),
uglify     = require('gulp-uglify'),
event      = require('event-stream');

let config = {
    bowerDir : "./frontend/app/bower_components",
    sourceDir: "./frontend/app",
    outputDir: "./frontend/dist"
};

// --- WATCH SERVER --- //

gulp.task('clean', function() {
    return gulp.src(config.outputDir, {read: false})
        .pipe(clean());
});

gulp.task('js', function() {
    return gulp.src(config.sourceDir + "/script/**/*.js")
        .pipe(concat("all.js"))
        .pipe(gulp.dest(config.outputDir + "/script"));
});

gulp.task('css', function() {
    return gulp.src(config.sourceDir + "/assets/css/*.css")
        .pipe(concat("all.css"))
        .pipe(gulp.dest(config.outputDir + "/assets/css"));
});

gulp.task('html', function() {
    let index = gulp.src(config.sourceDir + "/index.html")
        .pipe(gulp.dest(config.outputDir));

    let views = gulp.src(config.sourceDir + "/views/**/*.html")
        .pipe(gulp.dest(config.outputDir + "/views"));
});

gulp.task('fonts', function() {
    return gulp.src(config.sourceDir + "/assets/fonts/**/*")
        .pipe(gulp.dest(config.outputDir + "/assets/fonts"))
});

gulp.task('bower', function() {
    return gulp.src(config.bowerDir + "/**")
        .pipe(gulp.dest(config.outputDir + "/bower_components"));
});

gulp.task('images', function() {
    return gulp.src(config.sourceDir + "/assets/images/**/**")
        .pipe(gulp.dest(config.outputDir + "/assets/images"));
});

gulp.task('watch', function() {
    gulp.watch(config.sourceDir + "/**/*.html", ['html']);
    gulp.watch(config.sourceDir + "/script/**/*.js", ['js']);
    gulp.watch(config.sourceDir + "/assets/css/*.css", ['css']);
});

gulp.task('default', ['js', 'fonts', 'html', 'bower', 'images', 'css', 'watch']);
gulp.task('build', ['js', 'fonts', 'html', 'bower', 'images', 'css']);

// prevent load models to elasticsearch after create
process.env.SEED_DATA = true;

const serverConfig = require('./server/config/config');
const task = process.argv[2];
// set drop flag for complete db update
if (task === 'seed') {
    serverConfig.db.migrate = 'drop';
}