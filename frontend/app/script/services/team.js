(function (angular, app) {
    "use strict";

    app.service('teamService', [
        '$q',
        '$location',
        '$http',
        '$rootScope',
        'apiPath',
        'fieldbook',
        function (
            $q,
            $location,
            $http,
            $rootScope,
            apiPath,
            fieldbook
        ) {
            var data = {};
            var f = {
                data: data,
                getTeams: function () {
                    var data = {};
                    var deferred = $q.defer();

                    $http.get(apiPath + '/teams').then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                },
                addTeam: function (name, skills) {
                    var data = {};
                    var deferred = $q.defer();

                    var authorization = 'Basic ' + btoa(fieldbook.name + ':' + fieldbook.apiKey);

                    $http({
                        method: 'POST',
                        url: fieldbook.baseUrl + '/teams',
                        headers: {
                            'Authorization': authorization
                        },
                        data: {name: name, skills: skills}
                    }).then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                }
            };

            return f;
        }]);
})(angular, app);