(function (angular, app) {
    "use strict";

    app.service('userService', [
        '$q',
        '$location',
        '$http',
        '$rootScope',
        'apiPath',
        function (
            $q,
            $location,
            $http,
            $rootScope,
            apiPath
        ) {
            var data = {};
            var f = {
                data: data,
                login: function (username, password) {
                    var deferred = $q.defer();

                    $http.post(apiPath + '/auth', {
                        username: username,
                        password:password
                    }).then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                },
                getCurrent: function () {
                    var data = {};
                    var deferred = $q.defer();

                    $http.get(apiPath + '/users/current').then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                }
            };

            return f;
        }]);
})(angular, app);