(function (angular, app) {
    "use strict";

    app.service('goalScorerService', [
        '$q',
        '$location',
        '$http',
        '$rootScope',
        'apiPath',
        'fieldbook',
        function (
            $q,
            $location,
            $http,
            $rootScope,
            apiPath,
            fieldbook
        ) {
            var data = {};
            var f = {
                data: data,
                getGoalScorers: function () {
                    var data = {};
                    var deferred = $q.defer();

                    $http.get(apiPath + '/goalscorers').then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                },
                addGoalScorer: function (name, scores) {
                    var data = {};
                    var deferred = $q.defer();

                    var authorization = 'Basic ' + btoa(fieldbook.name + ':' + fieldbook.apiKey);

                    $http({
                        method: 'POST',
                        url: fieldbook.baseUrl + '/goalscorers',
                        headers: {
                            'Authorization': authorization
                        },
                        data: {name: name, scores: scores}
                    }).then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                }
            };

            return f;
        }]);
})(angular, app);