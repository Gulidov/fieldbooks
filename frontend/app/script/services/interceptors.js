(function (angular, app) {
    "use strict";

    app.factory('HttpInterceptor', ['$q','$window', function($q, $window) {

        return {
            'request': function(config) {
                return config;
            },
            'responseError': function(rejection) {

               if (rejection.status === 401){
                   //redirect to login if this not auth

                   //window.location.href = "/auth";
               }

                return $q.reject(rejection);
            }

        }
    }]);

})(angular, app);
