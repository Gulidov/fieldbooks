(function (angular, app) {
    "use strict";

    app.controller('teamController', [
        '$rootScope',
        '$scope',
        '$state',
        '$http',
        'teamService',
        function(
            $rootScope,
            $scope,
            $state,
            $http,
            teamService
        ) {
            $scope.teams = [];
            $scope.team = {
                name: '',
                skills: 0
            };
            $scope.init = function () {
                teamService.getTeams().then(
                    function (response) {
                        $scope.teams = response.data;
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
            };

            $scope.addTeam = function () {
                teamService.addTeam($scope.team.name, $scope.team.skills).then(
                    function (response) {
                        $scope.teams.push({name: $scope.team.name, skills: $scope.team.skills});
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
            };

            $scope.init();
        }]);


})(angular, app);