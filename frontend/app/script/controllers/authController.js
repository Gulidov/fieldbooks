(function (angular, app) {
    "use strict";

    app.controller('authController', [
        '$rootScope',
        '$scope',
        '$state',
        '$http',
        'userService',
        function(
            $rootScope,
            $scope,
            $state,
            $http,
            userService
        ) {
            $scope.user = {
                username: '',
                password: ''
            };
            $scope.auth = function () {
                userService.login($scope.user.username, $scope.user.password).then(
                    function (response) {
                        $state.go('root.authenticated.fieldbooks');
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
                return true;
            };
        }]);


})(angular, app);