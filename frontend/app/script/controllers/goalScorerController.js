(function (angular, app) {
    "use strict";

    app.controller('goalScorerController', [
        '$rootScope',
        '$scope',
        '$state',
        '$http',
        'goalScorerService',
        function(
            $rootScope,
            $scope,
            $state,
            $http,
            goalScorerService
        ) {
            $scope.goalscorers = [];
            $scope.goalscorer = {
                name: '',
                scores: 0
            };
            $scope.init = function () {
                goalScorerService.getGoalScorers().then(
                    function (response) {
                        $scope.goalscorers = response.data;
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
            };

            $scope.addGoalScorer = function () {
                goalScorerService.addGoalScorer($scope.goalscorer.name, $scope.goalscorer.scores).then(
                    function (response) {
                        $scope.goalscorers.push({name: $scope.goalscorer.name, scores: $scope.goalscorer.scores});
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
            };

            $scope.init();

        }]);


})(angular, app);