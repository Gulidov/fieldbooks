(function (angular, app) {
    "use strict";

    app.config(['$stateProvider', function ($stateProvider) {

        $stateProvider
            .state({
                name: 'root',
                url: '',
                abstract: true,
                views: {
                    "header": {
                        templateUrl: '/app/views/partials/header.html'
                    },
                    "footer": {
                        templateUrl: 'views/partials/footer.html'
                    }
                }
            })
            .state({
                url: '',
                name: 'root.authenticated',
                abstract: true,
                resolve:{
                    user:function(userService, $state) {
                        return userService.getCurrent().then(function(response) {
                        }).catch(function (response) {
                            if (response.status === 401) {
                                $state.go('root.auth');
                            }
                        });
                    }
                }
            })
            .state('root.auth', {
                url: "/auth",
                views: {
                    "content@": {
                        templateUrl: "views/auth.html"
                    }
                }
            });
    }]);

})(angular, app);
