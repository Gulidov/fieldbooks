(function (angular, app) {
    "use strict";

    app.controller('RootController', [
        '$rootScope',
        '$state',
        '$http',
        'userService',
        function(
            $rootScope,
            $state,
            $http,
            userService
        ) {
            userService.getCurrent().then(
                function (response) {
                    $state.go('root.authenticated.fieldbooks');
                }
            ).catch(
                function(res) {
                    $state.go('root.auth');
                }
            );
    }]);


})(angular, app);