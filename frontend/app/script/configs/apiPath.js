(function (angular, app) {
    'use strict';

    app.constant('apiPath', apiPath());

function apiPath () {

    return 'http://' + window.location.host + '/api/v1';
    //return 'http://redmine.cleveroad.com:3005/api/v1';

}

})(angular, app);