var app = {};

(function (angular){
    "use strict";

    var config = {
        'appVer': '',
        'apiVer': 1,
        'requires': ['ui.router', 'ui.bootstrap'],
        'path_info': {}
    };

    app = angular.module('fieldbooks', config['requires']);

})(angular);

(function (angular, app) {
    "use strict";

    app.config([
        '$stateProvider',
        '$urlRouterProvider',
        '$httpProvider',
        '$locationProvider',
        function (
            $stateProvider,
            $urlRouterProvider,
            $httpProvider,
            $locationProvider
        ) {
            $stateProvider
                .state('root.authenticated.fieldbooks', {
                    url: "/",
                    views: {
                        "content@": {
                            templateUrl: "views/fieldbooks.html"
                        }
                    }
                });

            //$locationProvider.html5Mode(true).hashPrefix('!');
            // $locationProvider.html5Mode({
            //     enabled: true,
            //     requireBase: false
            // });
            $httpProvider.interceptors.push('HttpInterceptor');

            $urlRouterProvider.when('', '/');
            // $urlRouterProvider.when('/', '/auth');
            // $urlRouterProvider.otherwise('/auth');
    }]);

})(angular, app);
