var app = {};

(function (angular){
    "use strict";

    var config = {
        'appVer': '',
        'apiVer': 1,
        'requires': ['ui.router', 'ui.bootstrap'],
        'path_info': {}
    };

    app = angular.module('fieldbooks', config['requires']);

})(angular);

(function (angular, app) {
    "use strict";

    app.config([
        '$stateProvider',
        '$urlRouterProvider',
        '$httpProvider',
        '$locationProvider',
        function (
            $stateProvider,
            $urlRouterProvider,
            $httpProvider,
            $locationProvider
        ) {
            $stateProvider
                .state('root.authenticated.fieldbooks', {
                    url: "/",
                    views: {
                        "content@": {
                            templateUrl: "views/fieldbooks.html"
                        }
                    }
                });

            //$locationProvider.html5Mode(true).hashPrefix('!');
            // $locationProvider.html5Mode({
            //     enabled: true,
            //     requireBase: false
            // });
            $httpProvider.interceptors.push('HttpInterceptor');

            $urlRouterProvider.when('', '/');
            // $urlRouterProvider.when('/', '/auth');
            // $urlRouterProvider.otherwise('/auth');
    }]);

})(angular, app);

(function (angular, app) {
    'use strict';

    app.constant('apiPath', apiPath());

function apiPath () {

    return 'http://' + window.location.host + '/api/v1';
    //return 'http://redmine.cleveroad.com:3005/api/v1';

}

})(angular, app);
(function (angular, app) {
    'use strict';

    app.constant('fieldbook', getConfigs());

    function getConfigs () {

        var bookId = '570f8b5921f9ec0300265b7b';

        return {
            bookId: bookId,
            baseUrl: 'https://api.fieldbook.com/v1/' + bookId,
            name: 'key-1',
            apiKey: 'ScOpBg-5s7q3P56UBR9W'
        };
    }

})(angular, app);
(function (angular, app) {
    "use strict";

    app.controller('authController', [
        '$rootScope',
        '$scope',
        '$state',
        '$http',
        'userService',
        function(
            $rootScope,
            $scope,
            $state,
            $http,
            userService
        ) {
            $scope.user = {
                username: '',
                password: ''
            };
            $scope.auth = function () {
                userService.login($scope.user.username, $scope.user.password).then(
                    function (response) {
                        $state.go('root.authenticated.fieldbooks');
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
                return true;
            };
        }]);


})(angular, app);
(function (angular, app) {
    "use strict";

    app.controller('fieldBookController', [
        '$rootScope',
        '$scope',
        '$state',
        '$http',
        function(
            $rootScope,
            $scope,
            $state,
            $http
        ) {
            $scope.teams = {};
            $scope.showingContent = '';
            $scope.init = function () {
            };

            $scope.init();

        }]);


})(angular, app);
(function (angular, app) {
    "use strict";

    app.controller('goalScorerController', [
        '$rootScope',
        '$scope',
        '$state',
        '$http',
        'goalScorerService',
        function(
            $rootScope,
            $scope,
            $state,
            $http,
            goalScorerService
        ) {
            $scope.goalscorers = [];
            $scope.goalscorer = {
                name: '',
                scores: 0
            };
            $scope.init = function () {
                goalScorerService.getGoalScorers().then(
                    function (response) {
                        $scope.goalscorers = response.data;
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
            };

            $scope.addGoalScorer = function () {
                goalScorerService.addGoalScorer($scope.goalscorer.name, $scope.goalscorer.scores).then(
                    function (response) {
                        $scope.goalscorers.push({name: $scope.goalscorer.name, scores: $scope.goalscorer.scores});
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
            };

            $scope.init();

        }]);


})(angular, app);
(function (angular, app) {
    "use strict";

    app.controller('teamController', [
        '$rootScope',
        '$scope',
        '$state',
        '$http',
        'teamService',
        function(
            $rootScope,
            $scope,
            $state,
            $http,
            teamService
        ) {
            $scope.teams = [];
            $scope.team = {
                name: '',
                skills: 0
            };
            $scope.init = function () {
                teamService.getTeams().then(
                    function (response) {
                        $scope.teams = response.data;
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
            };

            $scope.addTeam = function () {
                teamService.addTeam($scope.team.name, $scope.team.skills).then(
                    function (response) {
                        $scope.teams.push({name: $scope.team.name, skills: $scope.team.skills});
                    }
                ).catch(
                    function(res) {
                        console.log('fail');
                    }
                );
            };

            $scope.init();
        }]);


})(angular, app);
(function (angular, app) {
    "use strict";

    app.service('goalScorerService', [
        '$q',
        '$location',
        '$http',
        '$rootScope',
        'apiPath',
        'fieldbook',
        function (
            $q,
            $location,
            $http,
            $rootScope,
            apiPath,
            fieldbook
        ) {
            var data = {};
            var f = {
                data: data,
                getGoalScorers: function () {
                    var data = {};
                    var deferred = $q.defer();

                    $http.get(apiPath + '/goalscorers').then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                },
                addGoalScorer: function (name, scores) {
                    var data = {};
                    var deferred = $q.defer();

                    var authorization = 'Basic ' + btoa(fieldbook.name + ':' + fieldbook.apiKey);

                    $http({
                        method: 'POST',
                        url: fieldbook.baseUrl + '/goalscorers',
                        headers: {
                            'Authorization': authorization
                        },
                        data: {name: name, scores: scores}
                    }).then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                }
            };

            return f;
        }]);
})(angular, app);
(function (angular, app) {
    "use strict";

    app.factory('HttpInterceptor', ['$q','$window', function($q, $window) {

        return {
            'request': function(config) {
                return config;
            },
            'responseError': function(rejection) {

               if (rejection.status === 401){
                   //redirect to login if this not auth

                   //window.location.href = "/auth";
               }

                return $q.reject(rejection);
            }

        }
    }]);

})(angular, app);

(function (angular, app) {
    "use strict";

    app.service('teamService', [
        '$q',
        '$location',
        '$http',
        '$rootScope',
        'apiPath',
        'fieldbook',
        function (
            $q,
            $location,
            $http,
            $rootScope,
            apiPath,
            fieldbook
        ) {
            var data = {};
            var f = {
                data: data,
                getTeams: function () {
                    var data = {};
                    var deferred = $q.defer();

                    $http.get(apiPath + '/teams').then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                },
                addTeam: function (name, skills) {
                    var data = {};
                    var deferred = $q.defer();

                    var authorization = 'Basic ' + btoa(fieldbook.name + ':' + fieldbook.apiKey);

                    $http({
                        method: 'POST',
                        url: fieldbook.baseUrl + '/teams',
                        headers: {
                            'Authorization': authorization
                        },
                        data: {name: name, skills: skills}
                    }).then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                }
            };

            return f;
        }]);
})(angular, app);
(function (angular, app) {
    "use strict";

    app.service('userService', [
        '$q',
        '$location',
        '$http',
        '$rootScope',
        'apiPath',
        function (
            $q,
            $location,
            $http,
            $rootScope,
            apiPath
        ) {
            var data = {};
            var f = {
                data: data,
                login: function (username, password) {
                    var deferred = $q.defer();

                    $http.post(apiPath + '/auth', {
                        username: username,
                        password:password
                    }).then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                },
                getCurrent: function () {
                    var data = {};
                    var deferred = $q.defer();

                    $http.get(apiPath + '/users/current').then(
                        function (response) {
                            deferred.resolve(response);
                        }
                    ).catch(
                        function (err) {
                            deferred.reject(err);
                        }
                    );

                    return deferred.promise;
                }
            };

            return f;
        }]);
})(angular, app);
(function (angular, app) {
    "use strict";

    app.controller('RootController', [
        '$rootScope',
        '$state',
        '$http',
        'userService',
        function(
            $rootScope,
            $state,
            $http,
            userService
        ) {
            userService.getCurrent().then(
                function (response) {
                    $state.go('root.authenticated.fieldbooks');
                }
            ).catch(
                function(res) {
                    $state.go('root.auth');
                }
            );
    }]);


})(angular, app);
(function (angular, app) {
    "use strict";

    app.config(['$stateProvider', function ($stateProvider) {

        $stateProvider
            .state({
                name: 'root',
                url: '',
                abstract: true,
                views: {
                    "header": {
                        templateUrl: '/app/views/partials/header.html'
                    },
                    "footer": {
                        templateUrl: 'views/partials/footer.html'
                    }
                }
            })
            .state({
                url: '',
                name: 'root.authenticated',
                abstract: true,
                resolve:{
                    user:function(userService, $state) {
                        return userService.getCurrent().then(function(response) {
                        }).catch(function (response) {
                            if (response.status === 401) {
                                $state.go('root.auth');
                            }
                        });
                    }
                }
            })
            .state('root.auth', {
                url: "/auth",
                views: {
                    "content@": {
                        templateUrl: "views/auth.html"
                    }
                }
            });
    }]);

})(angular, app);
