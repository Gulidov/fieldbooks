# Api Backend

## Installation

1) nodejs - required v4.2.1 or above, install it with nvm https://github.com/creationix/nvm
2) npm - required v2.14.7 or above
3) mysql - - required v5.6 or above, to install run:

        sudo apt-get install mysql-server
    
## Usage


### Start, Stop, Restart, and Status

App uses [pm2](https://github.com/Unitech/pm2) module to manage all process related operational demands. All commands are assumed to be executed from project root.

- List current server(s) **status**:

        pm2 ls

- To **start** server:

        # with debugging on
        DEBUG=lg* pm2 start lift.js
        # without debugging
        pm2 start lift.js

- To **stop** or **restart** server:

        pm2 stop all
        pm2 restart all
        
To run application without `pm2` module:

        npm start

User for development and debugging:

         npm run dev
         npm run debug
         
## Workflow notes
